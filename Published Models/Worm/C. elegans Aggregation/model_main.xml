<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>Full title:        Caenorhabditis elegans L1 Aggregation
Date:              12.09.2022
Authors:         L. Avery, B. Ingalls, C. Dumur, A. Artyukhin
Contributor:  L. Brusch 
Software:       Morpheus (open-source), download from https://morpheus.gitlab.io 
Time unit:      second 
Space unit:    cm 
ModelID:        https://identifiers.org/morpheus/M7683
Comment:      Attractant+repellent Keller-Segel model, 1 cm x 1 cm, starting with 9.000 worms uniformly.
                         This model reproduces results from the following publication in the latest Morpheus version which were originally obtained with the same model in an older Morpheus version. 
Reference:      L. Avery, B. Ingalls, C. Dumur, A. Artyukhin. "A Keller-Segel model for C elegans L1 aggregation"
                         PLOS Computational Biology 17(7), e1009231, 2021.
                         https://doi.org/10.1371/journal.pcbi.1009231
</Details>
        <Title>Worm</Title>
    </Description>
    <Space>
        <Lattice class="square">
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
            <Size symbol="size" value="384, 384, 0"/>
            <BoundaryConditions>
                <Condition type="periodic" boundary="x"/>
                <Condition type="periodic" boundary="y"/>
            </BoundaryConditions>
            <NodeLength symbol="dx" value="0.0026041666666666665"/>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime symbol="tmax" value="200000"/>
        <TimeSymbol symbol="time"/>
        <RandomSeed value="123456"/>
    </Time>
    <Analysis>
        <ModelGraph include-tags="#untagged" format="svg" reduced="false"/>
        <Function symbol="Va">
            <Expression>-beta_a*log(alpha_a + Ua)</Expression>
        </Function>
        <Function symbol="Vaos2">
            <Expression>Va + beta_a*log(alpha_a)</Expression>
        </Function>
        <Function symbol="Vr">
            <Expression>-beta_r*log(alpha_r + Ur)</Expression>
        </Function>
        <Function symbol="Vros2">
            <Expression>Vr+beta_r*log(alpha_r+Ur)</Expression>
        </Function>
        <Function symbol="Vos2">
            <Expression>Vaos2+Vros2</Expression>
        </Function>
        <Gnuplotter time-step="treport" name="fields" decorate="true" file-numbering="time">
            <Plot title="Worms, V/σ">
                <!--    <Disabled>
        <Cells value="cell.clusterID"/>
    </Disabled>
-->
                <Cells value="cell.type"/>
                <Field symbol-ref="V" max="4" min="-3"/>
            </Plot>
            <Terminal name="png" size="2000,1000,0"/>
            <Plot>
                <Field symbol-ref="Ua" max="30000" min="0"/>
            </Plot>
            <Plot>
                <Field symbol-ref="Ur" max="9000" min="5000"/>
            </Plot>
        </Gnuplotter>
        <Gnuplotter time-step="treport" name="Vplot" decorate="true" file-numbering="time">
            <Plot>
                <Field symbol-ref="V"/>
            </Plot>
            <Terminal name="png" size="2000,2000,0"/>
            <Plot>
                <Field symbol-ref="Va"/>
            </Plot>
            <Plot>
                <Field symbol-ref="Vr"/>
            </Plot>
        </Gnuplotter>
        <Logger time-step="treport">
            <Input>
                <Symbol symbol-ref="cell.id"/>
                <Symbol symbol-ref="cell.center.x"/>
                <Symbol symbol-ref="cell.center.y"/>
                <Symbol symbol-ref="delta_r.x"/>
                <Symbol symbol-ref="delta_r.y"/>
                <Symbol symbol-ref="MKtemp"/>
                <Symbol symbol-ref="MKtime"/>
                <Symbol symbol-ref="cmstrength"/>
            </Input>
            <Output>
                <TextOutput file-name="worms" file-format="csv" header="true" separator="comma" file-numbering="time"/>
            </Output>
        </Logger>
        <Logger time-step="treport">
            <Input>
                <Symbol symbol-ref="space.x"/>
                <Symbol symbol-ref="space.y"/>
                <Symbol symbol-ref="Ua"/>
                <Symbol symbol-ref="Va"/>
                <Symbol symbol-ref="Ur"/>
                <Symbol symbol-ref="Vr"/>
                <Symbol symbol-ref="V"/>
            </Input>
            <Output>
                <TextOutput file-name="UaUr" file-format="csv" header="true" separator="comma" file-numbering="time"/>
            </Output>
        </Logger>
        <ClusteringTracker celltype="medium,worm" time-step="treport" exclude="cell.type==celltype.medium.id" name="Cluster size distribution">
            <ClusterID symbol-ref="cell.clusterID"/>
        </ClusteringTracker>
    </Analysis>
    <CPM>
        <Interaction>
            <Contact type2="worm" type1="worm" value="wormtoworm"/>
            <Contact type2="medium" type1="worm" value="wormtomedium"/>
        </Interaction>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
        </ShapeSurface>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration symbol="MKtime" value="0.15"/>
            <MetropolisKinetics temperature="MKtemp"/>
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
        </MonteCarloSampler>
    </CPM>
    <CellTypes>
        <CellType name="worm" class="biological">
            <ConnectivityConstraint/>
            <VolumeConstraint target="cellvolume" strength="1"/>
            <MotilityReporter time-step="1000" name="worm_motility">
                <Displacement symbol-ref="delta_r"/>
                <Velocity symbol-ref="vel"/>
            </MotilityReporter>
            <PropertyVector symbol="vel" value="0.0, 0.0, 0.0"/>
            <PropertyVector symbol="delta_r" value="0.0, 0.0, 0.0"/>
            <Chemotaxis strength="cmstrength" field="-V"/>
            <Property symbol="gridsa" value="sa/wormvolume"/>
            <Property symbol="gridsr" value="sr/wormvolume"/>
            <Property symbol="cell.clusterID" value="0"/>
        </CellType>
        <CellType name="medium" class="medium">
            <PropertyVector symbol="delta_r" value="0.0, 0.0, 0.0"/>
            <PropertyVector symbol="vel" value="0.0, 0.0, 0.0"/>
            <Property symbol="gridsa" value="0"/>
            <Property symbol="gridsr" value="0.0"/>
            <Property symbol="cell.clusterID" value="-1"/>
        </CellType>
    </CellTypes>
    <CellPopulations>
        <Population type="worm" size="360">
            <InitRectangle mode="random" number-of-cells="Nworms">
                <Dimensions origin="0.0, 0.0, 0.0" size="size.x, size.y, size.z"/>
            </InitRectangle>
        </Population>
    </CellPopulations>
    <Global>
        <Constant symbol="treport" value="1000"/>
        <Constant symbol="width" name="width" value="size.x*dx"/>
        <Constant symbol="dt" value="MKtime"/>
        <Constant symbol="nelements" name="nelements" value="size.x"/>
        <Constant symbol="sweep" name="sweep" value="1"/>
        <Constant symbol="MKtemp" value="10.0"/>
        <Field symbol="Ua" name="Attractant" value="0.0">
            <Diffusion rate="1e-6"/>
        </Field>
        <Field symbol="Ur" name="Repellent" value="0.0">
            <Diffusion rate="1e-5"/>
        </Field>
        <Field symbol="V" name="Combined potential" value="0.0"/>
        <Constant symbol="mu" value="MKtemp"/>
        <Constant symbol="cmstrength" value="mu"/>
        <Constant symbol="wormtoworm" value="0.0"/>
        <Constant symbol="wormtomedium" value="0.0"/>
        <Constant symbol="cellvolume" value="5"/>
        <Constant symbol="wormvolume" value="cellvolume*dx*dx"/>
        <Constant symbol="rho_bar" value="9000"/>
        <Constant symbol="Nworms" value="width*width*rho_bar"/>
        <ConstantVector symbol="delta_r" value="0.0, 0.0, 0.0"/>
        <ConstantVector symbol="vel" value="0.0, 0.0, 0.0"/>
        <Constant symbol="sa" value="0.01"/>
        <Constant symbol="s2" value="5.55555e-6"/>
        <Constant symbol="alpha_a" value="1500"/>
        <Constant symbol="beta_a" value="2"/>
        <Constant symbol="sr" value="0.001"/>
        <Constant symbol="alpha_r" value="1500"/>
        <Constant symbol="beta_r" value="-2"/>
        <System time-step="1" name="Dynamics dt=1 RK(4)" solver="Runge-Kutta [fixed, O(4)]">
            <DiffEqn symbol-ref="Ua">
                <Expression>gridsa - gamma_a*Ua</Expression>
            </DiffEqn>
            <Constant symbol="gamma_a" value="0.01"/>
            <DiffEqn symbol-ref="Ur">
                <Expression>gridsr-gamma_r*Ur</Expression>
            </DiffEqn>
            <Constant symbol="gamma_r" value="0.001"/>
            <Rule symbol-ref="V">
                <Expression>-beta_a*log(alpha_a + Ua)-beta_r*log(alpha_r + Ur)</Expression>
            </Rule>
        </System>
    </Global>
</MorpheusModel>
