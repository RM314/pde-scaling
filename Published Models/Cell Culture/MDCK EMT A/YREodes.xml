<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>
Naba Mukhtar, Eric N Cytrynbaum, and Leah Edelstein-Keshet (2022) A Multiscale computational model of YAP signaling in epithelial fingering behaviour

This file produced SI Figure 2.

Ordinary differential equations for the YAP-Rac-E cadherin circuit based on

Park, J., Kim, D.H., Shah, S.R., Kim, H.N., Kim, P., Quiñones-Hinojosa, A. and Levchenko, A., 2019. Switch-like enhancement of epithelial-mesenchymal transition by YAP through feedback regulation of WT1 and Rho-family GTPases. Nature communications, 10(1), pp.1-15.

With the given parameter values, the circuit is bistable. Change the initial YAP level to 0.5 to see the coexisting high YAP steady state.

</Details>
        <Title>YREodes</Title>
    </Description>
    <Time>
        <StartTime value="0"/>
        <StopTime value="20" symbol="stoptime"/>
        <TimeSymbol symbol="time"/>
        <RandomSeed value="1"/>
    </Time>
    <Space>
        <Lattice class="linear">
            <Size value="1, 0, 0" symbol="size"/>
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Global>
        <Variable value="0.1" name="YAP" symbol="Y"/>
        <Variable value="0.5" name="Rac1" symbol="R"/>
        <Variable value="0.5" name="E-cadherin" symbol="E"/>
        <System time-step="0.1" solver="Runge-Kutta [fixed, O(4)]">
            <Constant value="0.1" name="Basal rate of YAP activation" symbol="ky"/>
            <Constant value="1.8" name="E-cadherin-dependent rate of YAP deactivation" symbol="kye"/>
            <Constant value="1.8" name="Rac1-dependent rate of YAP activation" symbol="kyr"/>
            <Constant value="0.9" name="YAP-dependent rate of E-cadherin expression" symbol="ke"/>
            <Constant value="2" name="Inactivation rate of YAP" symbol="Dy"/>
            <Constant value="1" name="Inactivation rate of E-cadherin" symbol="De"/>
            <Constant value="0.5" name="Degradation rate of Rac1" symbol="Dr"/>
            <Constant value="0.9" name="Basal E-cadherin synthesis rate" symbol="Ce"/>
            <Constant value="1" name="Dissociation constant of YAP-WT1 transcriptional constant" symbol="K"/>
            <Constant value="3" name="Hill coefficient for E-cadherin" symbol="h"/>
            <Constant value="1" name="YAP-dependent rate of Rac1 expression" symbol="kr"/>
            <Constant value="0.5" name="Michaelis-Menten-like constant for Rac1" symbol="Kr"/>
            <Constant value="6" name="Hill coefficient for Rac1" symbol="n"/>
            <Constant value="1" name="Rac activation fraction" symbol="alphaR"/>
            <Constant value="0.0" symbol="Cr"/>
            <Constant value="1.2" name="Total YAP (active (Y) + inactive)" symbol="Ytot"/>
            <Constant value="5" name="Total Rac1 (active (R) + inactive)" symbol="Rtot"/>
            <DiffEqn name="Equation for YAP" symbol-ref="Y">
                <Expression>(ky+kyr*R)*(Ytot-Y) - (kye*Y*E + Dy*Y)</Expression>
            </DiffEqn>
            <DiffEqn name="Equation for E-cadherin" symbol-ref="E">
                <Expression>Ce - ke*Y^h/(K^h+Y^h) - De*E</Expression>
            </DiffEqn>
            <DiffEqn name="Equation for Rac1" symbol-ref="R">
                <Expression>alphaR*(Cr + kr*Y^n/(Kr^n+Y^n))*(Rtot-R) - Dr*R</Expression>
            </DiffEqn>
        </System>
    </Global>
    <Analysis>
        <Logger time-step="0.1">
            <Plots>
                <Plot title="Yap-Rac-Ecad Model" time-step="-1">
                    <Style decorate="true" grid="true" style="lines" line-width="2.0"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="time"/>
                    </X-axis>
                    <Y-axis minimum="0.0" maximum="4.0">
                        <Symbol symbol-ref="Y"/>
                        <Symbol symbol-ref="R"/>
                        <Symbol symbol-ref="E"/>
                    </Y-axis>
                </Plot>
            </Plots>
            <Output>
                <TextOutput/>
            </Output>
            <Input/>
        </Logger>
        <ModelGraph include-tags="#untagged" format="dot" reduced="false"/>
    </Analysis>
</MorpheusModel>
