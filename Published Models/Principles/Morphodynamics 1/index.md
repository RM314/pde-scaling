---
MorpheusModelID: M2051

authors: [N. Mulberry, L. Edelstein-Keshet]
contributors: [N. Mulberry]

published_model: original

title: "Two Layer Circuit"
date: "2021-03-24T04:40:00+01:00"
lastmod: "2021-07-26T19:43:00+02:00"

tags:
- Cadherin-1
- CDH1
- Cell Sorting
- Cellular Potts Model
- CPM
- Differential Adhesion
- E-cadherin
- Multiscale Models
- Notch Signaling Pathway
- Two-layer Structure

categories:
- DOI:10.1088/1478-3975/abb2dc
---
## Introduction

This model combines simple Notch signaling with the cellular Potts model (CPM), which drives cell sorting as a result of differential adhesion. Model details are contained in the reference [Mulberry and Keshet, 2020](#reference).

In this experiment, we consider a mixture of two distinct genotypes (a sender cell and a receiver cell), which become ‘activated’ and eventually self-organize into a stable two-layer structure.  

## Description

We define two cell types. Cell type A are the so-called sender cells, which produce the ligand Delta. Cell type B are the receiver cells, which produce the Notch receptor. A-type cells activate neighbouring B cells, thereby inducing the B cells to produce an E-cadherin protein. This promotes adhesion among activated B cells. 

![](schematic_twolayer.png)

The equation for ligand $D$ expressed by A-type sender cells is
$$ \frac{\mathrm dD}{\mathrm dt} = D_0 - \kappa_tN_\text{ext}D - \gamma D$$

Here $D$, produced at a constant rate $D_0$, is used up in binding to neighboring receiver Notch receptors $N_\text{ext}$, and has some inherent rate of decay.

The B-type receiver cells have Notch receptors $N$ that get activated by sender ligand $D_\text{ext}$. Upon activation, the Notch receptor releases its intracellular domain ($I$), which then activates gene expression and production of E-cad ($E$). Model equations for a receiver cell are:

$$\begin{align}
\frac{\mathrm dN}{\mathrm dt} &= N_0 \left(1 + \frac{I^p}{I_0^p+I^p}\right) - \kappa_tND_\text{ext}-\gamma N \\\\
\frac{\mathrm dI}{\mathrm dt} &= \kappa_tND_\text{ext}- \gamma_I I\\\\
\frac{\mathrm dE}{\mathrm dt} &= E_0 \frac{I^p}{I_0^p+I^p} - \gamma E\\\\
\end{align}$$

The ligand $D_\text{ext}$ of B neighbors trans-activates Notch at rate $\kappa_t$. Protein degradation occurs at rate $\gamma$ for Notch and E-cad, and at rate $\gamma I$ for $I$. 

## Results

In most computational experiments, we observe that the cells are able to self-organize into a two layer structure within $T = 200$ hours. Once sorted, the cells retain their ‘sender’ and ‘receiver’ characteristics, but we avoid any mechanism for irreversible cell-fate determination. Hence, the spatial distribution of the cells influences their dynamic interactions. We observe in simulations that, over longer time frames, the inner core of green cells starts to deactivate, and subsequently migrate outwards. However, once these deactivated cells reach the boundary of this core, they can once again become activated. This illustrates the stability of the two layer structure.

![](TwoLayerCircuit_Mov2.gif "Two Layer Circuit")

## Reference

This model is described in the peer-reviewed publication:

>N. Mulberry, L. Edelstein-Keshet: [Self-organized Multicellular Structures from Simple Cell Signaling: A Computational Model][reference]. *Phys. Biol.* **17**: 066003, 2020.

Our computational model was inspired by the experimental work of [Toda et al., 2018](https://science.sciencemag.org/content/361/6398/156). The Notch signaling model was based on that of [Boareto et al., 2015](https://www.pnas.org/content/112/5/E402.short).

[reference]: https://doi.org/10.1088/1478-3975/abb2dc