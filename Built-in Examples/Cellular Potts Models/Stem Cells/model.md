{{% callout note %}}
This model also requires the separate file [`crypt.tiff`](#downloads).
{{% /callout %}}

![](M0026_stem-cells_model-graph.svg "Model Graph")