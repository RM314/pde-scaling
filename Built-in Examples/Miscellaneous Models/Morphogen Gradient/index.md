---
MorpheusModelID: M0042

title: "French Flag: Morphogen Gradient"
date: "2019-11-10T10:08:00+01:00"
lastmod: "2020-10-30T13:48:00+01:00"

aliases: [/examples/french-flag-morphogen-gradient/]

menu:
  Built-in Examples:
    parent: Miscellaneous Models
    weight: 20
weight: 520
---
## Introduction

This example shows Wolpert's classical [French Flag model](https://en.wikipedia.org/wiki/French_flag_model). Depending on the local concentration of a morphogen, cells adopt one of three cell types based on internal thresholds.

![](french-flag.png "Wolpert´s French Flag.")

## Description

The model sets up a morphogen gradient in the $x$ direction as a 2D field in the ```Global``` section. Note that no diffusion is used, since we use the steady-state solution of the diffusion and degradation process with fixed boundary source.

The cells in ```CellType``` register the (average) local morphogen concentration using a ```Mapper```. Based on the specified threshold values, they choose an identity $I$ as defined in the ```Equation```.

Note that this model is not time-dependent. ```Time``` is therefore set from ```StartTime``` $0$ to ```StopTime``` $0$.

## Things to try

[//]: # (- Change the physical length of the domain by editing ```Space``` → ```NodeLength``` that controls the physical size per lattice site.)
- Change the model such that the morphogen gradient is set up by production and diffusion, using ```Diffusion```  and a ```System``` with ```DiffEqn```. That is, change the model into a time-dependent model.