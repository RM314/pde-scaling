---
MorpheusModelID: M0014

authors: [S. Miyazawa, M. Okamoto, S. Kondo]

title: "Spatial Parameter Sweep: Turing Patterns"
date: "2019-11-05T15:52:00+01:00"
lastmod: "2020-10-30T12:33:00+01:00"

aliases: [/examples/spatial-parameter-sweep-turing-patterns/]

menu:
  Built-in Examples:
    parent: PDE
    weight: 40
weight: 90
---

## Introduction

This model shows the pattern formation abilities of Turing's linear activator-inhibitor model ([Miyazawa *et al.*, 2010][miyazawa-2010]). It shows how to vary parameters as a function of space.

![](turing-miyazawa.png "Spots and stripes appear under various conditions in linear Turing model.")

## Description

Instead of fixed parameters defined as ```Constant```s, this model uses ```Function```s for two parameters. The parameters $C$ (activator production) and $A$ (rate of auto-activation) are defined as ```Function``` of space and varied over the $x$- and $y$-axes respectively. This requires the definition of a ```SpaceSymbol``` that can be used in expressions.

The results show the appearance of white spots (left), black spots (right) and labyrinthine patterns (middle).

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/47171578?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Reference

S. Miyazawa, M. Okamoto, S. Kondo: [Blending of animal colour patterns by hybridization][miyazawa-2010]. *Nat. Commun.* **1** (6): 66, 2010.

[miyazawa-2010]: http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2982180/