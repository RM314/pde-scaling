---
aliases:
- /models/
---
Since the Morpheus model repository was launched in 2021, it has been receiving a increasing number of new contributions. The **continuous growth** of the diverse model collection, sorted by submissions per model category, is shown below and updated daily:

![Development of model contributions over time sorted by categories](/media/model/stats/models-by-category.svg "Development of model contributions over time")

## Search Models

Use the **full-text search** field in the top-left of each category's page to find models by a specific author or cellular process.

![Screenshot showing the full-text search box for the Morpheus website in the model repo](faq/model-repository/search/model-search.png "Full-text search box for the Morpheus website in the model repository")

Moreover, each model is tagged with common **keywords** like model formalism which are displayed below each model description. Clicking these tags displays a list of other models (and further content on the Morpheus website) with the same tag.

![Screenshot showing the tag list at the end of a model page](faq/model-repository/search/model-tags.png "Alphabetically ordered tag list at the bottom of each model page")

## Cite Models

Each model (including contributed and built-in examples) is registered with a **unique and persistent ID of the format `M....`**. The model description page, the model's XML file and all further files connected with that model can be retrieved via a **persistent URL** of the format `https://identifiers.org/morpheus/M....`.

![Screenshot showing the expandable citable identifier link for a model](faq/model-repository/cite/model-id.png "Expandable citable unique identifier link for all models")

Further details on referencing, tracking of changes to the model etc. can be found in the [FAQ](/faq/models/referencing/).

## Submit Models

You can also **contribute to the Morpheus model repository** and upload MorpheusML-encoded model files yourself.

In our [FAQ](/faq/models/submit/) you will find all further information about this, including a **detailed guide** that takes you step by step through the **submission process**.

Also, please do not hesitate to **[contact us](/#support) directly with your model**. We are happy to guide you through the entire process until the final publication of your model here in the repository.

{{< cta cta_text="Contribute now!" cta_link="/faq/models/submit/" cta_new_tab="false" >}}