<MorpheusModel version="4">
    <Description>
        <Title>NotWavepinning</Title>
        <Details>Full title:		Wave-Pinning
Authors:		L. Edelstein-Keshet
Contributors:	Y. Xiao
Date:		22.06.2022
Software:		Morpheus (open-source). Download from https://morpheus.gitlab.io
Model ID:		https://identifiers.org/morpheus/M2011
File type:		Main model
Reference:		L. Edelstein-Keshet: Mathematical Models in Cell Biology
Comment:		Single GTPase model but with constant inactive GTPase. This simulation demonstrates that when there is no conservation of total (active plus inactive) GTPase, then there is no wave-pinning. The wave of activity takes over the entire domain. Credit: Lutz Brusch, for an earlier version of this code for the wave-pinning model with conservation.</Details>
    </Description>
    <Global>
        <Constant symbol="L" value="dx*size.x"/>
        <Function symbol="x">
            <Expression>dx*space.x</Expression>
        </Function>
        <Field symbol="a" name="active GTPase" value="2*exp(-x^2)">
            <Diffusion rate="0.1"/>
        </Field>
        <Field symbol="i" name="inactive GTPase" value="2">
            <Diffusion rate="10"/>
        </Field>
        <System solver="Dormand-Prince [adaptive, O(5)]">
            <DiffEqn symbol-ref="a">
                <Expression> i*(b+gamma*a^n/(1+a^n))- a  </Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="i">
                <Expression>0</Expression>
            </DiffEqn>
            <Constant symbol="gamma" name="feedback activation parameter" value="1"/>
            <Constant symbol="n" value="2"/>
            <Function symbol="b" name="basal activation rate">
                <Expression>0.067</Expression>
            </Function>
        </System>
    </Global>
    <Space>
        <Lattice class="linear">
            <Size symbol="size" value="100, 0, 0"/>
            <BoundaryConditions>
                <Condition type="noflux" boundary="x"/>
                <Condition type="noflux" boundary="-x"/>
            </BoundaryConditions>
            <NodeLength symbol="dx" value="0.1"/>
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="100"/>
        <TimeSymbol symbol="t" name="time"/>
    </Time>
    <Analysis>
        <Logger time-step="1">
            <Input>
                <Symbol symbol-ref="a"/>
                <Symbol symbol-ref="i"/>
                <Symbol symbol-ref="L"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot time-step="5" title="Spatial profiles">
                    <Style decorate="true" line-width="3.0" style="lines"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="x"/>
                    </X-axis>
                    <Y-axis maximum="2.5" minimum="0">
                        <Symbol symbol-ref="a"/>
                        <Symbol symbol-ref="i"/>
                    </Y-axis>
                    <Range>
                        <Time mode="current"/>
                    </Range>
                </Plot>
                <Plot time-step="-1" title="Time-space plot">
                    <Style style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="space.x"/>
                    </X-axis>
                    <Y-axis>
                        <Symbol symbol-ref="t"/>
                    </Y-axis>
                    <Color-bar>
                        <Symbol symbol-ref="a"/>
                    </Color-bar>
                </Plot>
            </Plots>
        </Logger>
        <ModelGraph format="svg" reduced="false" include-tags="#untagged"/>
    </Analysis>
</MorpheusModel>
