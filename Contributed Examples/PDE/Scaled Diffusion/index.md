---
MorpheusModelID: M7992

authors: [R. Müller]
contributors: [L. Brusch]

title: Scaled Diffusion Coefficient to Obtain Identical Results in Physical Units Independent of Lattice Discretization
date: "2023-10-03T10:42:00+02:00"
lastmod: "2024-01-04T14:49:00+01:00"

tags:
- 1D
- Activator
- Diffusion
- Discretization
- Inhibitor
- Lattice
- Partial Differential Equation
- PDE
- Scaling
- Turing

---

> This example model demonstrates how to obtain consistent results in physical units together with the freedom to choose arbitrary lattice and time discretizations.

## Introduction

Often, one starts to build a model with a particular lattice size and time scaling. Then later, one may want to run finer resolved simulations and has to increase the number of lattice nodes. However, that rather increases the spatial extensions and one needs to also rescale other parameters. This example model demonstrates how to do this right and strictly separate the physical scales from lattice and time discretizations. 

## Description

A PDE model is here defined in continuum space of size ```L.physical``` and discretized on the 1D interval ```[0,L.numerical]```, yielding a lattice spacing ```h = L.physical/L.numerical```. The spatial location in physical units is then available for expressions, reporting and plotting as ```x.physical = x.numerical.x*h```.

Likewise, the physical time interval ```[0,StopTime.physical]``` shall be discretized on the numerical interval ```[0,StopTime.numerical]``` with resulting physical time steps of duration ```tau = StopTime.physical/StopTime.numerical```. The physical time is then available for expressions, reporting and plotting as ```time.physical = time.numerical*tau```.
To analyse dynamic patterning due to reactions and diffusion, we added an activator-inhibitor model with fields ```a``` and ```i```. The rate constants and diffusion constants shall be given in physical units and then the fluxes in the differential equations inside ```Global/System``` are scaled with ```tau```.

The two diffusion coefficients ```D_a.physical``` and ```D_i.physical``` are given in physical units and then scaled inside ```Global/Field/Diffusion``` by the factor ```tau/(h*h)``` in response to the chosen discretizations.

## Results

Simulations show the emergence of a Turing pattern from the initial condition for the field ```a=x.physical/L.physical```. The profile of the activator variable is plotted as function of ```x.physical``` and ```time.physical```.
As expected, these results are independent of the chosen numerical discretizations.
Here we compare two runs with ```L.numerical = 80``` on the left vs. ```L.numerical = 800``` on the right. Beside small numerical errors due to the relatively coarse discretization in 
the first case, the patterns are the same in space and time.

![](logger.png "Snapshots for L.numerical=80 on left and L.numerical=800 on right with line color indicating time.")

![](kymograph.png "Kymograph for L.numerical=80 on left and L.numerical=800 on right with color indicating activator values.")
