---
MorpheusModelID: M2015

title: Non-local Chemical Signals

authors: [L. Edelstein-Keshet]
contributors: [Y. Xiao]

# Under review
hidden: false
private: false

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true
#  preprint: false

tags:
- Aggregation
- Attractant
- Attraction
- Cell Aggregation
- Cellular Potts Model
- Chemical Secretion
- Chemical Signals
- Chemoattraction
- Chemotaxis
- Cluster
- Concentration Field
- CPM
- Decay
- Difussion
- Keller-Segel Model
- Production
- Repellent
- Repulsion
- Secretion

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---
> Hybrid continuum-chemical and agent-based cell model

## Introduction

The idea that the diffusion of extracellular signaling molecules sets up non-local cellular communications can also be used to simulate non-local forces using chemoattraction and repulsion.

## Description

We compose a hybrid model with CPM cells that secrete two types of chemicals to which they respond chemotactically. We denote by <span title="//Global/Field[@symbol='c1']">$`c_1`$</span> the concentration field of the attractant, and by <span title="//Global/Field[@symbol='c2']">$`c_2`$</span> the repellent.

The is a simple generalization of the <a href="/model/m2010" title="<i class='fas fa-dna pr-1'></i>Morpheus Model ID: <code class='model-id-tooltip'>M2010</code>">‘Keller-Segel’ simulation</a> to have two rather than a single chemical. While the Keller-Segel model (with its pure attraction of cells) could produce cell aggregation under the appropriate conditions, the interplay of both attraction and repulsion cues can account for a greater variety of behaviors.


## Results

Cells each produce diffusible chemicals of two types, an attractant and a repellent. Depending on the relative rates of production, decay, and diffusion of attractant and repellent, the cells may either avoid each other (top), or cluster or aggregate (bottom).

![](CellGroupsNonlocMorpheus.png "Cell simulations with an attractant and a repellent chemical. Both chemicals diffuse, are produced by cells at rates <span title='//Global/System/Constant[@value=&#39;alpha1&#39;]'>$`\alpha_1`$</span>, <span title='//Global/System/Constant[@value=&#39;beta1&#39;]'>$`\beta_1`$</span> and decay at rates <span title='//Global/System/Constant[@value=&#39;alpha2&#39;]'>$`\alpha_2`$</span>, <span title='//Global/System/Constant[@value=&#39;beta2&#39;]'>$`\beta_2`$</span>. We see distinct behaviors, corresponding to a) pure repulsion of the cells, b) organized spacing, c) a few tight clusters, and d) a single large cluster of adherent cells.")

{{% callout note %}}
By adjusting the relative rates of diffusion (a.k.a. spatial ranges) and relative concentrations (a.k.a. magnitudes) of the corresponding ‘chemotactic forces’, we see distinct behaviors. **Try the following experiments** using model [`NonlocalChemSign.xml`](#model), which is set to behavior b) by default, to reproduce all behaviors shown in the figure above:

| Behavior | <span title="//Global/System/Constant[@value='alpha1']">$`\alpha_1`$</span> | <span title="//Global/System/Constant[@value='alpha2']">$`\alpha_2`$</span> | <span title="//Global/System/Constant[@value='beta1']">$`\beta_1`$</span> | <span title="//Global/System/Constant[@value='beta2']">$`\beta_2`$</span> | <span title="//Global/Field[@symbol='c1']/Diffusion/@rate">$`D_1`$</span> | <span title="//Global/Field[@symbol='c2']/Diffusion/@rate">$`D_2`$</span> |
|---|---|---|---|---|---|---|
| a) | $`0.5`$ | $`0.5`$ | $`1`$ | $`0.1`$ | $`0.5`$ | $`1`$ |
| **b)** | $`0.05`$ | $`0.05`$ | $`2`$ | $`0.5`$ | $`1`$ | $`0.5`$ |
| c) | $`4`$ | $`0.5`$ | $`1`$ | $`0.1`$ | $`0.5`$ | $`1`$ |
| d) | $`0.5`$ | $`0.05`$ | $`0.01`$ | $`0.5`$ | $`1`$ | $`0.5`$ |
{{% /callout %}}

<figure>
    ![](M2015_non-local-chemical-signals_video_behavior-b.mp4)
    <figcaption>
        Simulation video of {{< model_quick_access "media/model/m2015/NonlocalChemSign.xml" >}} with the preset behavior b).
    </figcaption>
</figure>
