---
MorpheusModelID: M2003

title: "One Random Walker"

authors: [L. Edelstein-Keshet]
contributors: [Y. Xiao]

# Under review
hidden: false
private: false

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true

tags:
- 2D
- Bacterium
- Biased Random Motion
- Biased Random Walk
- Chemical Bias
- Cellular Potts Model
- CPM
- DirectedMotion
- Directed Movement
- Directional Bias
- Disturbance
- Forward Direction
- Logger
- MCSDuration
- Random Motion
- Random Walk
- Random Walker
- Run
- Run and Tumble
- Single Cell
- StopTime
- Trajectory
- Tumble
- Unbiased Random Walk

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---
> Unbiased and biased random walkers

## Introduction

We can simulate the swimming motion of a cell, such as a bacterium, by interspersing short periods of directed motion with random turns or random scrambling of the cell’s velocity.

## Description

A cell is started at the center of the domain. At fixed time increments, the walker changes its direction of motion. Its velocity vector is then recomputed in an <span title="//CellTypes/CellType[@name='Walker']/Event[@name='Change direction']">`Event`</span> as a vector with random <span title="//CellTypes/CellType[@name='Walker']/Event/VectorRule/Expression[@value='rand_uni(-1,1), rand_uni(-1,1) , 1']">$`x`$ and $`y`$</span> components. A bias in the move direction can be introduced by setting <span title="//CellTypes/CellType[@name='Walker']/Property[@symbol='BiasOnOff']/@value">`BiasOnOff`</span> to $`0`$ (off) or $`1`$ (on).

## Results

Sample trajectories of a random walker with bias (right, <span title="//CellTypes/CellType[@name='Walker']/Property[@symbol='BiasOnOff']/@value">`BiasOnOff`&nbsp;$`= 1`$</span>) and without bias (left, <span title="//CellTypes/CellType[@name='Walker']/Property[@symbol='BiasOnOff']/@value">`BiasOnOff`&nbsp;$`= 0`$</span>) towards the <span title="//CellTypes/CellType[@name='Walker']/PropertyVector[@symbol='BiasDirection']/@value">$`y`$</span> direction. At fixed time increments, the walker changes its direction of motion, with new velocity vector recomputed with random $`x`$ and $`y`$ components. Time along the trajectories is color-coded from black to yellow.

![](OneRandomWalker.png "A random walker in 2D: a cell is started at the center of the domain. Produced with [`OneRandomWalker_main.xml`](#model).")

![](ChemicalBias.png "Left: Three sample snapshots of the cell and chemical concentration. Right: Cell trajectory over <span title='//Time/StopTime[@value=&#39;15000&#39;] and //CPM/MonteCarloSampler/MCSDuration[@value=&#39;1&#39;]'>$`15000\ \mathrm{MCS}`$</span>. The cell spends most of its time in the high <span title='//Global/Field[@symbol=&#39;c&#39;]'>$`c`$</span> region, as its run length drops and frequency of turning increases with <span title='//Global/Field[@symbol=&#39;c&#39;]'>$`c`$</span>. Produced with [`OneRandomWalkerWithChemicalBias.xml`](#downloads).")

<figure>
![](M2003_one-random-walker_video_OneRandomWalkerWithChemicalBias.mp4)
<figcaption>Simulation video of <a href="#downloads"><code>OneRandomWalkerWithChemicalBias.xml</code></a></figcaption>
</figure>