---
MorpheusModelID: M1175

contributors: [Robert Mueller]

title: "Chemical synapse"

tags:
- Contributed Examples
- 2D
- PDE
- Diffusion
- Cellular Potts Model
- CPM
- Field
- Field-Cell
- MembraneLattice
- Membrane
- MembraneProperty
- Multiscale
- Chemotaxis

# order
#weight: 20
---

>Membrane pattern allows anisotropic release of substance into a chemical synapse

## Introduction

Special cell types, like neurons or immune cells, communicate through a chemical synapse. This includes the anisotropic secretion of a substance into the gap between two cells.
This example serves to demonstrate a multiscale model of one cell releasing a substance into the medium and another cell close by sensing this substance. 
Please also see the in-app docu for ```GUI / Documentation / MorpheusML / Space/MembraneLattice```, ```.../ CellType / MembraneProperty``` and ```.../ CellType / Chemotaxis```. 
This example was motivated by Kevin Thurley (personal communication) and is derived from the more general demo model
[Anisotropic release from cell](https://morpheus.gitlab.io/model/m5482/).

## Description

A cell in Morpheus can act in an anisotropic manner by using a heterogeneously patterned ```MembraneProperty``` - here called ```m```. These are ```Properties``` with a spatial resolution 
(a 1D or 2D array, defined under Space/MembraneLattice) that are mapped to the cell’s membrane in polar coordinates in 2D or 3D, respectively. One can use this to represent cell polarity and give directionality to processes. 

In Morpheus, ```Space / MembraneLattice``` and subsequently ```CellType / MembraneProperty``` ```m``` are specified. One can use this as if it were a normal ```Property```, but one that 
is spatially  heterogeneous along the cell membrane. For technical reasons, ```m``` then has to be cast into a global field ```m2field``` using ```CellType / Mapper```.

In case of substance release into the chemical synapse it is relevant to limit the release to the border pixels of the cell. The second property 
```contact2medium``` is obtained via ```Global / NeighbourhoodReporter``` and indicates that the respective pixels belong to the boundary of a cell.

The product ```m2field*contact2medium``` enters the ```Global / System``` for the secreted (and diffusing and degraded) substance ```c```, limiting release of ```c``` to cell 
boundary pixels and to a direction given by the pattern in ```m```.

For demonstration purposes, the sensing cell (red in movie) calculates the average of the ```c``` concentration and writes it to an ```Analysis / Logger```  

Position and movement of the cells are artificially constructed for demonstration purposes only. Chemotaxis (```Chemotaxis plugin```) to the special shaped technical helper 
fields U2 and U3 approximately keeps the cells at a defined vertical (```y```) value without hampering horizontal fluctuations too much. 
The sensing cell (red in movie) has a weak additional chemotactic term to the field c, keeping it near the releasing cell (blue in movie). 

Movie visualising the secretion of a substance into a chemical synapse using MembraneLattice:
![Movie visualising the secretion of a substance into a chemical synapse using MembraneLattice.](chemical_synapse.mp4)

The two panels of the same simulation show the global field concentration ```c``` (left) and the membrane concentration ```m``` of the substance (right) over time.

